[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687161.svg)](https://doi.org/10.5281/zenodo.4687161)

# country dependent profile for each NUTS0 region for electricity demand (WP 2.7)

In this repository the yearlong_2014_2015_2016_2017 load profile for electricity demand, modelled as part of WP 2.7, is published. 
However, as not all countries publish data regarding hourly electricity demand/supply, some years/countries are missing.
Data regarding electricity demand is available from Open Power System Data (http://www.open-power-system-data.org/) and licensed at Open Power System Data under an MIT license.
Data is not altered with respect to the original source. For use in analyses and models, please mind the S/W time changeover at hour 2090.

## Repository structure

Files:
```
data/hotmaps_task_2.7_load_profile_electricity_demand_yearlong_2014_2015_2016_2017.csv                 -- contains the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## References
[1] [Open Power System Data](https://doi.org/10.25832/time_series/2018-06-30) Data Package Time series,  2018, Version 2018-06-30. Primary data from various sources, for a complete list see URL.



## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramn Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Muller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Kuhnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report  Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors

Matthias Kuehnbach, Simon Marwitz, Anna-Lena Klingler <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://isi.fraunhofer.de/)
Fraunhofer ISI, Breslauer Str. 48, 
76139 Karlsruhe


## License


Copyright  2016-2018: Matthias Kuehnbach, Anna-Lena Klingler, Simon Marwitz

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
